const calculateWinner = (arr) => {
  const winCombinations = [
    // Rows
    ['0', '1', '2'],
    ['3', '4', '5'],
    ['6', '7', '8'],
    // Columns
    ['0', '3', '6'],
    ['1', '4', '7'],
    ['2', '5', '8'],
    // Diagonal
    ['0', '4', '8'],
    ['2', '4', '6'],
  ]

  let cells = [...arr[0], ...arr[1], ...arr[2]]

  for (let comb of winCombinations) {
    if (
      cells[comb[0]] !== null &&
      cells[comb[0]] === cells[comb[1]] &&
      cells[comb[1]] === cells[comb[2]]
    ) {
      return cells[comb[0]]
    }
  }
  return null
}

const getGameState = (field) => {
  let gameState = ''
  const winner = calculateWinner(field)

  const isTie = () => !winner && !field.flat().includes(null)
  const gameIsOver = () => winner || !field.flat().includes(null)

  if (gameIsOver()) {
    if (winner) {
      gameState = `${winner === 'x' ? 'Крестики победили' : 'Нолики победили'}`
    }
    if (isTie()) {
      gameState = 'Ничья'
    }
  } else {
    gameState = 'Следующий ход'
  }

  // console.log(gameState)
  alert(gameState)
}

// крестики победили:
const gameField1 = [
  ['x', 'o', null],
  ['x', null, 'o'],
  ['x', 'o', 'o'],
]
// Нолики победили:
const gameField2 = [
  ['x', 'o', 'x'],
  [null, 'o', 'x'],
  [null, 'o', null],
]
// Ничья:
const gameField3 = [
  ['x', 'o', 'x'],
  ['x', 'x', 'o'],
  ['o', 'x', 'o'],
]
// Игра идет - Следующий ход:
const gameField4 = [
  ['x', 'o', null],
  [null, null, null],
  [null, null, null],
]

getGameState(gameField1)
getGameState(gameField2)
getGameState(gameField3)
getGameState(gameField4)
